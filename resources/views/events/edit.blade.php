@extends('layouts.master')

@section('content')
<link rel="stylesheet" href="">
<style>
    textarea {
        resize: none;
    }
</style>
    <div class="row">
        <div class="col-md-8 mx-auto my-4">
            <div class="card">
                <div class="card-body">
				<form action="/events/{{ $event->title }}/updateAll" method="post">
                    @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
		
                    <div class="col-sm-8 font-weight-bold float-right">
                      <label for="Description">Description:</label>
                      <textarea class="form-control " name="description" id="description"  rows="14" resize:none>{{ $event->description }}</textarea>
                    </div>

                    <div class="col-sm-4 font-weight-bold">
                        <div class="form-group">
                            <label for="Title">Title:</label>
                            <input type="text" name="title" placeholder="Title" class='form-control'
                                value="{{ $event->title }}">
                        </div>
                    </div>

                    <div class="col-sm-4 font-weight-bold">
                        <div class="form-group">
                            <label for="Date Started">Date Started:</label>
                            <input type="date" name="start_date" placeholder="Date Started" class='form-control'
                                value="{{ $event->start_date }}">
                        </div>
                    </div>

                    <div class="col-sm-4 font-weight-bold">
                        <div class="form-group">
                            <label for="Date End">Date End:</label>
                            <input type="date" name="end_date" placeholder="Date End" class='form-control'
                                value="{{ $event->end_date }}">
                        </div>
                    </div>

                    <div class="col-sm-4 font-weight-bold">
                        <div class="form-group">
                            <label for="Facility Category">Facility Category</label>
                            <select name="facility_category_id" id="" class="form-control" required>
                                @foreach($facility_categories as $category)
                                    {{-- check if category id matches --}}
                                    @if($category->id == $event->facility_category_id)
                                        {{-- yes then select the option --}}
                                        <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                                    @else
                                        {{-- no then just display it --}}
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>

				</form>
                </div>
            </div>
        </div>
    </div>
@endsection