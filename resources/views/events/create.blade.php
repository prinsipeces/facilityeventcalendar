@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-8 mx-auto my-4">
            <div class="card">
                <div class="card-header bg-dark text-white">
                    <h5 class="card-title p-0 m-0">Add an Event</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('events.store')}}" method="post">
                        @csrf
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="container">
                            <fieldset class="form-group row">
                                <legend class="col-form-legend col-sm-1-12">Title</legend>
                                <div class="col-sm-1-12">
                                    <input type="text" class="form-control" name="title" id="title" placeholder="Title">
                                </div>
                            </fieldset>

                            <fieldset class="form-group row">
                                <legend class="col-form-legend col-sm-1-12">Description</legend>
                                <div class="col-sm-1-12">
                                    <textarea class="form-control" name="description" id="description" placeholder="Description"></textarea>
                                </div>
                            </fieldset>

                                <div class="form-group row">
                                    <legend class="col-form-legend col-sm-1-12">Facility Category</legend>
                                    <select name="facility_category_id" id="" class="form-control">
                                        @foreach($facility_categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            <fieldset class="form-group row">
                                <legend class="col-form-legend col-sm-1-12">Date Started</legend>
                                <div class="col-sm-1-12">
                                    <input type="date" class="form-control" name="start_date" id="start_date" placeholder="Date Started">
                                </div>
                            </fieldset>

                            <fieldset class="form-group row">
                                <legend class="col-form-legend col-sm-1-12">Date End</legend>
                                <div class="col-sm-1-12">
                                    <input type="date" class="form-control" name="end_date" id="end_date" placeholder="Date End">
                                </div>
                            </fieldset>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection