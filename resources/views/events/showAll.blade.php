@extends('layouts.master')
@section('content')
	<div class="card">
		<div class="card-header">
			<h5>Equipment</h5>
		</div>
		<div class="card-body">
			<a href = '/events/create' class="btn btn-primary">Add an Event</a>
			<hr>

			<div class="col-sm-12 d-flex justify-content-end">
				<form action="/search" method="get">
					<div class="form-group d-flex justify-content-end">
						<input type="text" name="search" class="form-control" placeholder="Search">
						<span class="form-group-btn col-sm-3 d-flex justify-content-end">
							<button type="submit" class="btn btn-primary">Search</button>
						</span>
						<span class="form-group-btn sm-2 d-flex justify-content-end">
							<a href="showAll" class="btn btn-dark">Reset</a>
						</span>
					</div>
				</form>
			</div>


			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Title</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Facility</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($events as $event)
						<tr>
							<td>{{ $event->title }}</td>
							<td>{{ $event->start_date }}</td>
							<td>{{ $event->end_date }}</td>
							<td>{{ $event->facility_category->name }}</td>
							<td>
								<a href="/events/{{ $event->title }}" class="btn btn-primary">View</a>
								<a href="/events/{{ $event->title }}/edit" class="btn btn-info">Edit</a>
								<a href="/events/{{ $event->title }}/delete" class="btn btn-danger">Delete</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>

			
		</div>
	</div>
@endsection