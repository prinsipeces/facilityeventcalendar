@extends('layouts.master')

@section('content')
<link rel="stylesheet" href="">
<style>
    textarea {
        resize: none;
    }
</style>

    <div class="row">
        <div class="col-md-8 mx-auto my-4">
            <div class="card">
                <div class="card-body">

                    <div class="col-sm-8 font-weight-bold float-right">
                      <label for="Description">Description:</label>
                      <textarea class="form-control " name="description" id="description"  rows="14" resize:none disabled>{{ $event->description }}</textarea>
                    </div>


                    <div class="col-sm-4 font-weight-bold">
                        <div class="form-group">
                            <label for="Title">Title:</label>
                            <input type="text" name="title" placeholder="Title" class='form-control text-danger'
                                value="{{ $event->title }}" disabled>
                        </div>
                    </div>

                    <div class="col-sm-4 font-weight-bold">
                        <div class="form-group">
                            <label for="Date Started">Date Started:</label>
                            <input type="date" name="start_date" placeholder="Date Started" class='form-control text-danger'
                                value="{{ $event->start_date }}" disabled>
                        </div>
                    </div>

                    <div class="col-sm-4 font-weight-bold">
                        <div class="form-group">
                            <label for="Date End">Date End:</label>
                            <input type="date" name="end_date" placeholder="Date End" class='form-control text-danger'
                                value="{{ $event->end_date }}" disabled>
                        </div>
                    </div>

                    <div class="col-sm-4 font-weight-bold">
                        <div class="form-group">
                            <label for="Facility Category">Facility:</label>
                            <input type="text" name="facility_category_id" placeholder="Facility Category" class='form-control text-danger'
                                value="{{ $event->facility_category->name }}" disabled>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <a href="javascript:history.back()" class="btn btn-primary">Go Back</a>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection