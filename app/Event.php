<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $guarded = [];
     protected $fillable = ['title', 'description', 'facility_category_id','start_date','end_date'];

    public function getRouteKeyName(){
        return 'title';
    }

    public function facility_category()
    {
    	return $this->belongsTo(FacilityCategory::class);
    }
    
}
