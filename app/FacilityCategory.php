<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacilityCategory extends Model
{
    //
	protected $guarded = [];
	
    public function event()
    {
    	return $this->hasMany(Event::class);
    }
}
