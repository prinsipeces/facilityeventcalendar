<?php

namespace App\Http\Controllers;

use App\Event;
use App\FacilityCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return $this->eventsToArray(Event::all());
    }

    public function eventsToArray($events){
        $eventsArray = [];
        foreach($events as $event){
            $data = [
                "title" => $event->title,
                "start" => $event->start_date,
                "end" => $event->end_date,
                "textColor" => "white"
            ];
            array_push($eventsArray,$data);
        }
        return response()->json($eventsArray);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $facility_categories = FacilityCategory::all();
        return view('events/create', compact('facility_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the form
        request()->validate([
            'title' => 'required',
            'description' => 'required',
            'facility_category_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'        
        ]);
        //store the model
        Event::create([
            "title" => $request->title,
            "description" => $request->description,
            "facility_category_id" => $request->facility_category_id,
            "start_date" => $request->start_date,
            "end_date" => $request->end_date
        ]);
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
        return view('events/show')->withEvent($event);
    }

     public function showAll()
    {
        //
        $events = Event::all();
        $facility_categories = FacilityCategory::all();
        return view('events/showAll', compact('events', 'facility_categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
        
        $facility_categories = FacilityCategory::all();
        return view('events/edit', compact('event', 'facility_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        //
        $event->update([
            "start_date" => $request->start,
            "end_date" => $request->end
        ]);
        return response()->json(['success'=>'updated']);
    }

    public function updateAll(Event $event)
    {
        request()->validate([
            'title' => 'required',
            "description" => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'facility_category_id' => 'required'
        ]);
        $event->update(request()->only([
            'title', 'description', 'start_date', 'end_date', 'facility_category_id'
        ]));

        return redirect('/showAll');
    }

        

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
     public function destroy(Event $event)
    {
         $event->delete();
        return response()->json(['success'=>'deleted']);
    }

    public function delete(Event $event)
    {

        $event->delete();
        return redirect('/showAll');
    }

     public function search(Request $request)
    {
         $events = Event::get();
        $search = \Request::get('search'); 
        $events = Event::where('title','like','%'.$search.'%')
            ->orWhereHas('facility_category', function ($query) use ($search) {
                $query->where('name', 'like', '%'.$search.'%');
            })
            ->orderBy('id')
            ->paginate(4);

        return view('events.showAll', compact('events'));
        
    }


}

?>


