<?php

use App\FacilityCategory;
use Illuminate\Database\Seeder;

class FacilityCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //insert 5 categories
        $categories = [
        	['name' => 'Theater'],
        	['name' => 'Gym'],
        	['name' => 'P.E Function Hall'],
        	['name' => 'Auditorium'],
        	['name' => 'T.B.I'],
        	['name' => 'Library']
        ];
        FacilityCategory::insert($categories);
    
    }
}
