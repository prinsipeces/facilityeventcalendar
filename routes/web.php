<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'LoginController@index')->name('login');
Route::post('/login', 'LoginController@store');

Route::middleware('auth')
	->group(function() {
        //logincontroller
        //logout
		Route::get('/logout', 'LoginController@logout');
		//change password
        Route::get('/changepassword','LoginController@showChangePasswordForm');
        Route::post('/changepassword','LoginController@changePassword')->name('changepassword');

		//eventcontroller
        Route::get('/', function () {
		    return view('/events/index');
		});
		Route::resource('/events','EventController');
		//show all events
		Route::get('/showAll','EventController@showAll');
		//edit event
		Route::post('/events/{event}/updateAll', 'EventController@updateAll');
		//delete one event
		Route::get('/events/{event}/delete', 'EventController@delete');
		//search
		Route::get('/search','EventController@search');






});
